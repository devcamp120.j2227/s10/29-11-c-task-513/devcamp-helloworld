import logo from "./assets/images/logo-og.png";

const gDevcampReact = {
    title: 'Chào mừng đến với Devcamp React',
    image: logo,
    benefits: ['Blazing Fast', 'Seo Friendly', 'Reusability', 'Easy Testing', 'Dom Virtuality', 'Efficient Debugging'],
    studyingStudents: 20,
    totalStudents: 100,
    countPercentStudyingStudent() {
      return this.studyingStudents / this.totalStudents * 100;
    }
}

export default gDevcampReact;